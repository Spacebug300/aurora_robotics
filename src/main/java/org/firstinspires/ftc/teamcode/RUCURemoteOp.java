/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.util.Range;


/**
 * This operation mode is required by the robot (RUCU) so it can be controlled with the gamepad. The control
 * scheme is "tank mode". Each analogue stick (for gamepad 1) controls the left and right drive motor group. This way
 * the driver can control both the speed of the robot and the direction by adjusting the speed of each motor group.
 *
 * Controller number 2 (gamepad 2) is used to control all features of the robot which does not involve moving the robot
 * itself. The left stick is used to adjust the height of the arm, the d-pad is used to switch the arm between two
 * states (opened 90 degrees; closed 0 degrees). The right analogue stick is used to control the lift hook of the
 * robot, up and down on the analogue stick correspond to the motion of the motor on the robot.
 *
 * This Opmode was written by AURORA 2018-2019.
 */

@TeleOp(name="RUCU: Remote User Op", group="RUCU")
public class RUCURemoteOp extends LinearOpMode {

    // Declare OpMode members.
    RUCUHardware robot = new RUCUHardware();
    private ElapsedTime runtime = new ElapsedTime();

    @Override
    public void runOpMode() {
        telemetry.addData("[STATUS] ", "Remote user operation Initialized.");
        telemetry.update();

        robot.init(hardwareMap);

        //Set the claw position to 0.0 in the beginning
        double clawState = 0.0;

        // Wait for the game to start (driver presses PLAY)
        waitForStart();
        runtime.reset();

        //Run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {

            //CONTROLLER 1 DEFINITIONS START

            //While loop check if the sticks are being used, its purpose is to smooth the movement of the
            //robot
            while (gamepad1.left_stick_y != 0 || gamepad1.right_stick_y != 0) {

                //Each analog stick on gamepad 1 controls each motor on either side of the bot.
                //Power is limited to 50% because otherwise it is too fast or unstable.
                double leftPower  = Range.clip(-gamepad1.left_stick_y, -0.3, 0.3);
                double rightPower = Range.clip(-gamepad1.right_stick_y, -0.3, 0.3);

                //Send calculated power to wheels
                robot.leftDrives(leftPower);
                robot.rightDrives(rightPower);

            }


            while (gamepad1.dpad_right || gamepad1.dpad_left) {

                //Use d-pad stick to control translation movement.
                boolean transRight = gamepad1.dpad_right;
                boolean transLeft = gamepad1.dpad_left;
                if (transLeft) {
                    robot.rightTop.setPower(1.0);
                    robot.rightBot.setPower(-1.0);
                    robot.leftTop.setPower(-1.0);
                    robot.leftBot.setPower(1.0);
                } else if (transRight) {
                    robot.rightTop.setPower(-1.0);
                    robot.rightBot.setPower(1.0);
                    robot.leftTop.setPower(1.0);
                    robot.leftBot.setPower(-1.0);
                }

            }
            // CONTROLLER 1 DEFINITIONS END



            //CONTROLLER 2 DEFINITIONS START
            double liftControl = gamepad2.right_stick_y;
            double liftPower = Range.clip(liftControl, -0.5, 0.5);

            boolean clawOpen = gamepad2.dpad_left;
            boolean clawClose = gamepad2.dpad_right;

            //The not statement is used to make sure the claw is not being set twice at once
            if (clawOpen && clawState <= 1) {
                telemetry.addData("Claw opening!", clawState);
                clawState += 0.05;
            } else if (clawClose && clawState >= 0) {
                telemetry.addData("Claw closing!", clawState);
                clawState -= 0.05;
            }

            robot.claw.setPosition(clawState); //Update claw position based on previous `if` statement.

            robot.lift.setPower(liftPower);

            double armControl = -gamepad2.left_stick_y;
            double armPower = Range.clip(armControl, -0.5, 0.5); //limit arm operation speed for stability.
            robot.arm.setPower(armPower);
            //CONTROLLER 2 DEFINITIONS END

            robot.allDrives(0.0); //Set all moving drive motors to 0 speed.
            telemetry.addData("Servo desired position: ", clawState);
            telemetry.addData("Servo current position: ", robot.claw.getPosition());
            telemetry.update();
        }
    }
}
