package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.hardware.modernrobotics.ModernRoboticsI2cColorSensor;

/**
 * This file is used to declare all of RUCU's on-board hardware. THIS IS NOT AN OPMODE
 *
 *
 * RUCU CURRENT CONFIGURATION:
 * Motors:
 * leftBot      = movement motor on the left bottom of the chassis
 * rightBot     = movement motor on the right bottom of the chassis
 * leftTop      = movement motor on the top left of the chassis
 * rightTop     = movement motor on the top right of the chassis
 * lift         = motor controlling lifting hook
 * arm          = motor controlling claw height
 *
 * Servos:
 * claw
 *
 * Sensors:
 * colorSensor
 *
 * ===MAKE SURE THIS FILE IS KEPT UP TO DATE WITH EVERY NEW ADDITION OF HARDWARE===
*/

public class RUCUHardware {
    //Motors
    public DcMotor leftBot;
    public DcMotor rightBot;
    public DcMotor leftTop;
    public DcMotor rightTop;
    public DcMotor lift;
    public DcMotor arm;

    //Servos
    public Servo claw;

    //Sensors
    public ModernRoboticsI2cColorSensor colorSensor;

    public RUCUHardware() {

    }

    HardwareMap hwMap;
    public void init(HardwareMap ahwMap) {
        hwMap = ahwMap;

        //Motor definitions
        leftBot = hwMap.get(DcMotor.class, "leftBot");
        rightBot = hwMap.get(DcMotor.class, "rightBot");
        leftTop = hwMap.get(DcMotor.class, "leftTop");
        rightTop = hwMap.get(DcMotor.class, "rightTop");
        lift = hwMap.get(DcMotor.class, "lift");
        arm = hwMap.get(DcMotor.class, "arm");

        leftBot.setDirection(DcMotor.Direction.REVERSE);
        leftTop.setDirection(DcMotor.Direction.REVERSE);

        //Servo Definitions
        claw = hwMap.get(Servo.class, "claw");


        //Sensor Definitions
        colorSensor = hwMap.get(ModernRoboticsI2cColorSensor.class, "colorSensor");

    }

    public void allDrives(double spd) {
        leftBot.setPower(spd);
        rightBot.setPower(spd);
        leftTop.setPower(spd);
        rightTop.setPower(spd);
    }

    public void rightDrives(double spd) {
        rightBot.setPower(spd);
        rightTop.setPower(spd);
    }

    public void leftDrives(double spd) {
        leftBot.setPower(spd);
        leftTop.setPower(spd);
    }

    public void rearDrives(double spd) {
        leftBot.setPower(spd);
        rightBot.setPower(spd);
    }

    public void fwdDrives(double spd) {
        leftTop.setPower(spd);
        rightTop.setPower(spd);
    }
}


