/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;


/**
 * This is the operation mode which will be used for the autonomous section of the competition for RUCU.
 *
 * It begins by unhooking the robot from its starting position. Once it is on the ground, it will
 * power on its wheels to translate away from the hooking position. The mission we will preform
 * first is the sampling mission. The robot approaches the sampling mission, after which it will
 * translate left until the color sensor detects a color of value 7 (yellowish). It will then drive
 * into it (push the gold block) and the program will terminate.
 *
 *
 * This opmode was written by AURORA 2018-2019.
 */

@Autonomous(name="RUCU: Autonomous Crater", group="RUCU")
public class RUCUAutonomousCrater extends LinearOpMode {

    // Make instance of hardware collection
    RUCUHardware robot = new RUCUHardware();

    //declare runtime
    private ElapsedTime runtime = new ElapsedTime();
    @Override
    public void runOpMode() {
        telemetry.addData("[STATUS] ", "Running Autonomous operation mode.");
        telemetry.update();

        robot.init(hardwareMap);

        waitForStart();
        runtime.reset();
        robot.colorSensor.enableLed(true); // turn on light for sensing normal objects, turn off for sensing lit up objects.


        while (opModeIsActive()) {

            //lift arm before dropping
            robot.arm.setPower(0.5);
            sleep(1000);
            robot.arm.setPower(0.0);

            //Drop from hook
            robot.lift.setPower(-0.5);
            sleep(1600);
            robot.lift.setPower(0.0);

            sleep(200); //These pauses are added between all movement segments to make sure the robot's movements are precise.

            //Move left to release the hook
            robot.rightTop.setPower(1.0);
            robot.rightBot.setPower(-1.0);
            robot.leftTop.setPower(-1.0);
            robot.leftBot.setPower(1.0);
            sleep(500);
            robot.allDrives(0.0);

            /*Move backwards to align against the wall
            robot.allDrives(-0.50);
            sleep(750);
            robot.allDrives(0.0);
            */

            sleep(200);

            //Move forward to drop off Shaggy.
            robot.allDrives(1.0);
            sleep(800);
            robot.allDrives(0.0);

            sleep(200);

            robot.leftDrives(-1.0);
            robot.rightDrives(1.0);
            sleep(300);
            robot.allDrives(0.0);

            sleep(200);

            robot.allDrives(1.0);
            sleep(1000);
            robot.allDrives(0.0);

            sleep(200);

            robot.leftDrives(-1.0);
            robot.rightDrives(1.0);
            sleep(100);
            robot.allDrives(0.0);

            sleep(200);

            robot.allDrives(1.0);
            sleep(2000);
            robot.allDrives(0.0);

            sleep(500);

            robot.leftDrives(-1.0);
            robot.rightDrives(1.0);
            sleep(700);
            robot.allDrives(0.0);

            sleep(200);

            robot.allDrives(1.0);
            sleep(2500);
            robot.allDrives(0.0);

            sleep(200);





            /**
            //translate to the right of the sampling area (reset position)
            robot.rightTop.setPower(-1.0);
            robot.rightBot.setPower(1.0);
            robot.leftTop.setPower(1.0);
            robot.leftBot.setPower(-1.0);
            sleep(800);
            robot.allDrives(0.0);

            sleep(200);

            //Wait until the color sensor detects a color number of 7 (yellowish color)
            while(robot.colorSensor.readUnsignedByte(ModernRoboticsI2cColorSensor.Register.COLOR_NUMBER) != 8 || robot.colorSensor.readUnsignedByte(ModernRoboticsI2cColorSensor.Register.COLOR_NUMBER) != 9 || robot.colorSensor.readUnsignedByte(ModernRoboticsI2cColorSensor.Register.COLOR_NUMBER) != 10) {

                telemetry.addData("Color Value: ", robot.colorSensor.readUnsignedByte(ModernRoboticsI2cColorSensor.Register.COLOR_NUMBER));
                telemetry.update();
                robot.rightTop.setPower(0.5);
                robot.rightBot.setPower(-0.5);
                robot.leftTop.setPower(-0.5);
                robot.leftBot.setPower(0.5);

            }

            robot.allDrives(0.0);

            sleep(200);

            //drive into the gold
            robot.allDrives(0.25);
            sleep(1000);
            robot.allDrives(0.0);
            */

            break;
        }
    }
}
