This is the TeamCode repository which is part of the ftc_app-master folder.

This repository contains all the code that our team has written and once downloaded is to be
placed inside the already existing ftc_app-master folder. The files have been split up like this for the
sake of usability. It is much easier to copy just the custom code into an already-installed version of
ftc_app-master because the iml files in the folder once launched in Android Studio are modified
to work with the machine they are being edited in.

# Please remember to **PULL** *before editing* and to **PUSH** *after editing*!
# Once cloned into ftc_app-master rename this folder to "TeamCode"
